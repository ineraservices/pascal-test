#language: sv

Egenskap: 13-Läkemedelslista Dos/Receptutskrift Pascal
  Läkemedelslista Dos/Receptutskrift Pascal är i första hand till för
  vårdpersonal för att få tillgång till utskrift av patientens aktuella
  förskrivningar. Underlag för utdelning av läkemedel.

  Bakgrund:
    Givet att jag är inloggad i Pascal som Sjuksköterska
    Och dos-patient är hämtad

  Scenario: 13-1 Skriva ut Läkemedelslista Dos/Receptutskrift Pascal
    Givet jag väljer skriva ut Receptutskrift Pascal
    Så visas dialogen "Utskrifter"
    Och jag klickar "Välj"
    # Här ska verifieras rätt personnummer visas
    Och jag klickar tillbaka till Pascalsida
    Och jag loggar ut
