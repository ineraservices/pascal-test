#language: sv

Egenskap: 3-Förskriva
  Att förskriva är central funktionalitet i applikationen.

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    När Pascal föreslår förpackning och antalet uttag för helförpackningsrecept är vald
    Och dos-patient är hämtad
    Och jag klickar "Ny förskrivning"

    Scenario: 3-1 Förskriva Disp 12 tillfällen
      Givet att jag söker efter läkemedel "Elvanse 30 mg"
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Scenariotestfall_3-1"
      När jag anger kortnotation "1X12"
      Och jag anger utsättning efter 28 dagar
      Så visas kortnotation "1x12 i 28d"
      Och jag anger förmån "Nej"
      Och meddelande om villkor för förmån visas
      Och jag klickar "Skapa & stäng"
      Så finns ett framtidrecept med innehåll:
        | Elvanse | 30 mg | Kapsel, hård |  08 1 + 09 1 + 10 1 + 11 1 + 12 1 + 13 1 + 15 1 + 16 1 + 17 1 + 18 1 + 19 1 + 20 1 | Scenariotestfall_3-1 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-2 Förskriva Disp 7 dagar 2 tillfälle
      Givet att jag söker efter läkemedel "Tolterodin 1 mg"
      Så visas äldrevarning
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Scenariotestfall_3-2"
      Och jag väljer oregelbunden dosering med schema "Annan oregelbunden dosering"
      Och jag väljer när schemat nått sista dagen, ska schemat "inte upprepas"
      Och jag klickar "Välj oregelbunden dosering"
      # Valet "inte upprepas" borde inte finnas för 7-dagars schema.
      # Veckoschema ska inte användas för upp- och nertrappning.
      Och jag väljer antal dagar i schema "6 dagar"
      Och jag fyller i för alla dagar i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        | 1  |    | 1  |    |
      Så visas information om doseringen "6 dagars schema, tabletter. Dag 1, 2, 3, 4, 5 och 6: 1 kl 8, 1 kl 16"
      Och jag klickar "Skapa & stäng"
      Så finns ett framtidrecept med innehåll:
        | Tolterodin Sandoz | 1 mg | Filmdragerad tablett | 08 D + 16 D | oregelbunden | Scenariotestfall_3-2 |
      Och receptet är markerad som ny
      Och äldrevarning är aktiv
      Och jag loggar ut

    Scenario: 3-3 Förskriva Disp akut
      Givet att jag söker efter läkemedel "Brufen"
      #Olika testpatienter får olika sortiment för Dispenserbara läkemdel
      #Och jag väljer "Alvedon" "Filmdragerad tablett" "500 mg" från lista
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Scenariotestfall_3-3"
      När jag anger kortnotation "1x2 i 6m"
      Och jag väljer akut produktion
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
        | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      #  | Paracetamol NET | 500 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      #  | Alvedon | 500 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-4 Förskriva Disp 3 ggr/v
      Givet att jag söker efter läkemedel "Digoxin"
      Och jag väljer "Digoxin BioPhausia" "Tablett" "0,25 mg" från lista
      Och jag markerar "Får ej bytas"
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Scenariotestfall_3-4"
      Och jag väljer oregelbunden dosering med schema "Tre gånger per vecka"
      Och jag fyller i för alla dagar i doseringsschemat:
        | 08  |
        | 1   |
      Och jag anger insättning 2 dagar innan första dag i dosperiod
      Och jag accepterar akut produktion om det krävs för valt datum
      Och jag anger giltighetstid 6 månader
      Och jag klickar "Skapa & stäng"
      Så finns ett framtidrecept med innehåll:
        | Digoxin BioPhausia | 0,25 mg | Tablett | 08 D | oregelbunden | Scenariotestfall_3-4 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-5 Förskriva Disp med upptrappning
      Givet att jag söker efter läkemedel "Bisoprolol"
      Och jag väljer "Bisoprolol Sandoz" "Filmdragerad tablett" "1,25 mg" från lista
      Och jag markerar "Får ej bytas"
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Behandling av kronisk hjärtsvikt, Scenario_3-5-1"
      Och jag väljer oregelbunden dosering med schema "Annan oregelbunden dosering"
      Och jag väljer när schemat nått sista dagen, ska schemat "inte upprepas"
      Och jag klickar "Välj oregelbunden dosering"
      Och jag väljer antal dagar i schema "14 dagar"
      Och jag fyller i för dag 1 till 7 i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        |    | 1  |    |    |
      Och jag fyller i för dag 8 till 14 i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        |    | 2  |    |    |
      Och jag väljer akut produktion
      Och jag lägger till kommentar till vårdpersonal "Symtom kan redan inträffa under den första dagen efter insättning av behandlingen. Om detta tolereras väl, öka enligt instruktionen."
      Och jag klickar "Skapa & förskriv igen"
      Så finns ett aktuellt recept med innehåll:
        | Bisoprolol Sandoz | 1,25 mg | Filmdragerad tablett | 12 D | oregelbunden | Behandling av kronisk hjärtsvikt, Scenario_3-5-1 |
      Och receptet är markerad som ny
      När jag söker efter läkemedel "Bisoprolol"
      Så visas dublettvarning
      Och jag väljer "Bisoprolol Sandoz" "Filmdragerad tablett" "1,25 mg" från lista
      Och jag markerar "Får ej bytas"
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Behandling av kronisk hjärtsvikt, Scenario_3-5-2"
      Och jag anger kortnotation "3 kl12"
      Och jag anger insättning 14 dagar efter dagens datum
      Och jag accepterar akut produktion om det krävs för valt datum
      Och jag anger utsättning efter 7 dagar
      Och jag lägger till kommentar till vårdpersonal "Symtom kan redan inträffa under den första dagen efter insättning av behandlingen. Om detta tolereras väl, öka enligt instruktionen."
      Och jag klickar "Skapa & stäng"
      Så finns ett framtidrecept med innehåll:
        | Bisoprolol Sandoz | 1,25 mg | Filmdragerad tablett | 12 3 | Behandling av kronisk hjärtsvikt, Scenario_3-5-2 |
      Och receptet är markerad som ny
      Och dubblettvarning är inte aktiv
      Och jag loggar ut

    Scenario: 3-6 Förskriva HF, VB
      Givet jag väljer helförpackning
      När jag väljer läkemedel "Waran"
      Och jag anger ändamål "Scenariotestfall_3-6"
      Och jag anger kortnotation "1+0+0+1"
      Och jag markerar "Vid behov"
      Och jag väljer förpackning
      Och jag väljer antal uttag 4
      Och jag väljer antal fp 2
      Och jag väljer Startförpackning
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
        | Waran | 2,5 mg | Tablett | 1 tablett kl 08 och 1 tablett kl 20 vid behov | Scenariotestfall_3-6 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-7 Förskriva HF, fritext dosering
      Givet jag väljer helförpackning
      När jag väljer läkemedel "Oxascand 25 mg"
      Och jag anger ändamål "Scenariotestfall_3-7"
      Och jag anger kortnotation "1x4"
      När jag söker efter läkemedel "Oxas"
      Och jag väljer "Oxascand och Sobril" "Tablett" "15 mg" från lista
      Och jag klickar "Välj läkemedel"
      Så visas dialogen "Läkemedelsspecifikation ändrad"
      Och jag klickar "Töm dosering"
      Och jag anger dosering i fritext "15 mg 3-4 gånger dagligen, som vid behov kan ökas till 30 mg 3-4 gånger dagligen."
      Och jag markerar "Vid behov"
      Och jag anger utsättning efter 6 månader
      Och jag väljer förpackning
      Och jag väljer antal uttag 4
      Och jag väljer antal fp 1
      Och jag anger expeditionsinterval 1 månader
      Och jag lägger till kommentar till vårdpersonal "Efter längre tids användning bör dosen minskas gradvis för att undvika utsättningsfenomen."
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
        | Oxascand | 15 mg | Tablett | 15 mg 3-4 gånger dagligen, som vid behov kan ökas till 30 mg 3-4 gånger dagligen. | Scenariotestfall_3-7 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-8 Förskriva Extempore
      Givet att jag väljer helförpackning
      När jag söker efter läkemedel "Extempore"
      Och jag klickar länken icke-godkända läkemedel
      Och jag markerar "Får ej bytas"
      Och jag klickar "Välj läkemedel"
      Och jag anger läkemedelsspecifikation "Läkemedlets namn, form, styrka och total mängd"
      Och jag anger ändamål "Scenariotestfall_3-8"
      Och jag väljer doseringsenhet "ml"
      Och jag väljer oregelbunden dosering med schema "Annan oregelbunden dosering"
      Och jag väljer när schemat nått sista dagen, ska schemat "inte upprepas"
      Och jag klickar "Välj oregelbunden dosering"
      Och jag väljer antal dagar i schema "8 dagar"
      Och jag fyller i för dag 1 till 1 i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        | 1  |    |    |    |
      Och jag fyller i för dag 3 till 3 i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        | 2  |    |    |    |
      Och jag fyller i för dag 5 till 5 i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        | 3  |    |    |    |
      Och jag fyller i för dag 7 till 7 i schemat för annan orgelbunden dosering:
        | 08 | 12 | 16 | 20 |
        | 4  |    |    |    |
      Och jag väljer antal uttag 1
      Och jag väljer antal fp 1
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
        | Extempore | Läkemedlets namn, form, styrka och total mängd | 08 D | oregelbunden | Scenariotestfall_3-8 |
      Och receptet är markerad som ny
      Och receptet har EXT-symbolen
      Och jag loggar ut

    Scenario: 3-9 Förskriva Licensläkemedel
      Givet jag markerar "Sök även bland icke-godkända läkemedel"
      Och jag väljer helförpackning
      När jag söker efter läkemedel "6700"
      Och jag rensar sökfältet
      Och jag söker efter läkemedel "Canesten"
      Och jag väljer "Apocanda-Lösung" "Kutan lösning" "" från lista
      Och jag markerar "Får ej bytas"
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Scenariotestfall_3-9"
      Och jag väljer oregelbunden dosering med schema "Varannan dag"
      Och jag fyller i för alla dagar i doseringsschemat:
        | 08 |
        | 1  |
      Och jag anger utsättning efter 31 dagar
      Och jag väljer förpackning
      Och jag väljer antal uttag 1
      Och jag väljer antal fp 2
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
        | Apocanda-Lösung | Kutan lösning | 08 1 | varannan dag | Scenariotestfall_3-9 |
      Och receptet är markerad som ny
      Och receptet har LIC-symbolen
      Och jag loggar ut

    Scenario: 3-10 Förskriva HF, Stående
      Givet jag väljer helförpackning
      När jag söker efter läkemedel "Detrusitol 2 mg"
      Så visas dublettvarning
      Och jag väljer "Detrusitol och 2 likvärdiga" "Filmdragerad tablett" "2 mg" från lista
      Och jag klickar "Välj läkemedel"
      Och jag anger ändamål "Scenariotestfall_3-10"
      När jag anger kortnotation "1 kl 20"
      Och jag väljer förpackning "Detrusitol" "60 tabletter"
      Så föreslår Pascal 4 uttag 2 fp
      Och jag klickar "Skapa & stäng"
      Så visas dialogen "Bekräfta dubbelförskrivning"
      Och jag klickar "Bekräfta"
      Så finns ett aktuellt recept med innehåll:
       | Detrusitol | 2 mg | Filmdragerad tablett | 20 1 | Scenariotestfall_3-10 |
      Och receptet är markerad som ny
      Och äldrevarning är aktiv
      Och jag loggar ut
