#language: sv

Egenskap: 7-Sätt ut
  Sätt ut recept

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    Och jag noterar antalet historiska recept

  Scenario: 7-1 Sätt ut direkt ett dispenserad aktuellt recept
    Och Patientinformation fliken är vald
    Och jag klickar "Ändra patientinformation och status"
    Och jag väljer expediering "Vilande"
    Och jag anger orsak "Semester"
    Och jag klickar "Spara och stäng"
    Och patienten är markerad med status "Vilande"
    Och jag går till fliken "Läkemedel"
    Givet att det finns ett recept med innehåll:
      | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
    När jag väljer "Sätt ut" i menyn för receptet
    Så visas dialogen "Sätt ut"
    Och dialogtexten innehåller "På det här receptet finns en ändring som ännu inte trätt i kraft, eftersom förändringen ska börja gälla i framtiden. Vid utsättning kommer även den framtida ändringen att tas bort."
    Och dialogtexten innehåller "Tidigare planerad utsättning"
    Och jag anger utsättningsorsak "Överkänslighet"
    Och jag markerar "Sätt ut direkt"
    Så visas dialogen "Utsättning innan sista dosdag"
    Och jag klickar "Beställ ny leverans"
    Så är "Ny leverans krävs" markerad
    Och ikonen för interaktionsvarning visas
    När jag klickar på interaktionsvarning
    Så visas dialogen "Varningsöversikt för Brufen"
    Och dialogtexten innehåller "mot Waran"
    Och jag klickar "Stäng"
    Och jag klickar "Sätt ut"
    Och dialogtexten för dosaktivering visas "Bekräfta aktivering av produktion av dosrullar"
    Och jag klickar "Bekräfta"
    Så finns inte något recept med innehåll:
      |  Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
    Och antalet historiska recept har ökat med 1
    Och det finns minst ett historiskt recept som har slutdatum idag, med innehåll:
      | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Utsatt: Överkänslighet |
    Och jag loggar ut

  Scenario: 7-2 Sätt ut en framtid dispenserad recept
    Givet att det finns ett recept med innehåll:
      | Digoxin BioPhausia | 0,25 mg | Tablett | 08 D | oregelbunden | Scenariotestfall_3-4 |
    När jag väljer "Sätt ut" i menyn för receptet
    Så visas dialogen "Sätt ut"
    Och dialogtexten innehåller "På det här receptet finns en ändring som ännu inte trätt i kraft, eftersom förändringen ska börja gälla i framtiden. Vid utsättning kommer även den framtida ändringen att tas bort."
    Och dialogtexten innehåller "Tidigare planerad utsättning"
    Och jag anger utsättningsorsak "Synonym, byte till ett synonympreparat"
    Och jag anger utsättningsdatum idag
    Så visas dialogen "Utsättning innan sista dosdag"
    Och jag klickar "Beställ ny leverans"
    Så är "Ny leverans krävs" markerad
    Och utsättningstid är "24:00"
    Och jag klickar "Sätt ut"
    Så finns inte något recept med innehåll:
      | Digoxin BioPhausia | 0,25 mg | Tablett | 08 D | oregelbunden | Scenariotestfall_3-4 |
    Och antalet historiska recept har ökat med 1
    Och det finns minst ett historiskt recept som har slutdatum idag, med innehåll:
      | Digoxin BioPhausia | 0,25 mg | Tablett | 08 D | oregelbunden | Utsatt: Byte till ett synonympreparat |
    Och jag loggar ut

  Scenario: 7-3 Sätt ut en aktuell dispenserad recept
    Givet att det finns ett recept med innehåll:
      | Bisoprolol Sandoz | 1,25 mg | Filmdragerad tablett | 12 D | oregelbunden | Behandling av kronisk hjärtsvikt, Scenario_3-5-1 |
    När jag väljer "Sätt ut" i menyn för receptet
    Så visas dialogen "Sätt ut"
    Och dialogtexten innehåller "Tidigare planerad utsättning"
    Och jag anger utsättningsorsak "Dubblett"
    Och jag anger utsättningsdatum idag
    Så visas dialogen "Utsättning innan sista dosdag"
    Och jag klickar "Stäng"
    Så är "Ny leverans krävs" inte markerad
    Och utsättningstid är "24:00"
    Och jag klickar "Sätt ut"
    Så finns ett aktuellt recept med innehåll:
      | Bisoprolol Sandoz | 1,25 mg | Filmdragerad tablett | 12 D | oregelbunden | Behandling av kronisk hjärtsvikt, Scenario_3-5-1 |
    Och receptets utsättningsdatum är idag
    Och antalet historiska recept är oförändrat
    Och jag loggar ut

  Scenario: 7-4 Sätt ut en aktuell Licensläkemedel
    Givet att det finns ett recept med innehåll:
      | Apocanda-Lösung | Kutan lösning | 08 1 | varannan dag | Scenariotestfall_3-9 |
    När jag väljer "Sätt ut" i menyn för receptet
    Så visas dialogen "Sätt ut"
    Och meddelande om en aktiv beställning visas
    Och dialogtexten innehåller "Tidigare planerad utsättning"
    Och utsättningstid är aktuell tid
    Och "Sätt ut direkt" är markerad
    Och jag anger utsättningsorsak "Annan orsak"
    Och jag anger utsättningsorsak i fritext "Scenariotestfall_7-4"
    När jag anger utsättningsdatum om 2 dagar
    Så är "Sätt ut direkt" inte markerad
    Och utsättningstid är "24:00"
    Och jag klickar "Sätt ut"
    Så finns ett aktuellt recept med innehåll:
      | Apocanda-Lösung | Kutan lösning | 08 1 | varannan dag | Scenariotestfall_3-9 |
    Och receptets utsättningsdatum är om 2 dagar
    Och jag loggar ut
