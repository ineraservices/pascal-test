#language: sv

Egenskap: 3_2-FörskrivaHv
  Att förskriva Handelsvaror

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    Och Handelsvaror fliken är vald
    Och jag klickar "Ny förskrivning"
  
  Scenario: 3-11 Förskriva HV teknisk sprit, VB
    När jag söker efter handelsvara "Teknisk sprit"
    Och jag väljer handelsvara "Teknisk Sprit E-Förskrivning" "640000" "" från lista
    Och jag klickar "Lägg till"
    Och jag anger handelsvara ändamål "Scenariotestfall_3-11"
    Och jag markerar "Vid behov"
    Och jag anger uttag 3
    Och jag anger förpackningar 2
    Så visas valideringsmeddelande "Du får endast ange 1 uttag vid förskrivning av teknisk sprit"
    Och jag anger uttag 1
    Och jag anger patientinstruktioner "Vid rengörning av sår"
    Och jag klickar "Skapa & stäng"
    Så finns ett aktuellt recept med innehåll:
     | Teknisk Sprit E-Förskrivning | Vid rengörning av sår | Scenariotestfall_3-11 |
    Och receptet är markerad som ny
    Och jag loggar ut

    Scenario: 3-12 Förskriva HV Cutisoft, Stående
      När jag söker efter handelsvara "Cutisoft"
      Och jag väljer handelsvara "Cutisoft Wipes" "204364" "" från lista
      Och jag klickar "Lägg till"
      Och jag anger handelsvara ändamål "Scenariotestfall_3-12"
      Och jag anger uttag 4
      Och jag anger förpackningar 1
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
       | Cutisoft Wipes | Scenariotestfall_3-12 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-13 Förskriva HV Natriumklorid, VB
      När jag söker efter handelsvara "Spol"
      Och jag väljer handelsvara "Natriumklorid Microspol" "210352" "" från lista
      Och jag klickar "Lägg till"
      Och jag anger handelsvara ändamål "Scenariotestfall_3-13"
      Och jag markerar "Vid behov"
      Och jag anger uttag 4
      Och jag anger förpackningar 2
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
       | Natriumklorid Microspol | Scenariotestfall_3-13 |
      Och receptet är markerad som ny
      Och jag loggar ut

    Scenario: 3-14 Förskriva HV Freedom Spruta, VB
      När jag söker efter handelsvara "Freedom Spruta"
      Och jag väljer handelsvara "Freedom Spruta" "736563" "" från lista
      Och jag klickar "Lägg till"
      Och jag anger handelsvara ändamål "Scenariotestfall_3-14"
      Och jag markerar "Vid behov"
      Och jag anger uttag 12
      Och jag anger förpackningar 4
      Och jag klickar "Skapa & stäng"
      Så finns ett aktuellt recept med innehåll:
       | Freedom Spruta | Scenariotestfall_3-14 |
      Och receptet är markerad som ny
      Och jag loggar ut
