#language: sv

Egenskap: 12-Meddelande
  Skicka meddelande är en funktion i Pascal receptsverktyget som
  behörig kunna skapa ett meddelande som förskrivare angående en patient
  till dosapoteket.

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    Och Meddelande fliken är vald

  Scenario: 12-1 Skicka ett nytt meddelande
    Och jag klickar "Nytt meddelande"
    Och jag anger ärende "Scenariotestfall_12-1"
    # ska verifieras steg 2
    Och jag skriver meddelande "Hej Testare"
    # ska verifieras steg 3
    Och jag klickar "Skicka"
    # ska verifieras steg 4
    Och jag loggar ut
