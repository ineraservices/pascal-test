#language: sv

@abort_on_failure
Egenskap: 0-Återställa
  Används för att återställa efter eventuella tidigare tester

  Scenario: 0-0 Testinfo
    Givet att jag är inloggad i Pascal som Läkare
    Och jag öppnar information om Pascal
    Så visas applikationens version
    Och jag skriver ut testvariabler

  Scenario: 0-1 Rensa beställningar dos-patient
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    När jag går till fliken "Beställningar"
    Och jag avbeställer alla aktiva beställningar
    Så är alla beställningar avbeställda

  Scenario: 0-2 Rensa läkemedel dos-patient
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    När jag makulerar alla framtida recept
    Och jag sätter ut alla återstående recept
    Så finns inga aktuella eller framtida recept

  Scenario: 0-3 Rensa handelsvaror dos-patient
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    När jag går till fliken "Handelsvaror"
    Och jag makulerar alla makulerbara recept
    Så finns inga aktuella eller framtida recept

  Scenario: 0-4 Rensa förbered icke dos-patient
    Givet att jag är inloggad i Pascal som Sjuksköterska
    Och icke-dos-patient är hämtad
    När jag rensar förberedd data

  Scenario: 0-5 Återställ inställningar
    Givet att jag är inloggad i Pascal som Läkare
    När Pascal föreslår förpackning och antalet uttag för helförpackningsrecept är vald
