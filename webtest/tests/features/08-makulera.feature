#language: sv

Egenskap: 8-Makulera
  Om ett recept är felaktig skall den kunna makuleras

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad
    Och jag noterar antalet historiska recept

  Scenario: 8-1 Makulera en framtida dispenserad recept
    Givet att det finns ett recept med innehåll:
      | Elvanse | Scenariotestfall_3-1 |
    När jag väljer "Makulera" i menyn för receptet
    Och jag väljer makuleringsorsak "Fel patient"
    Och jag klickar "Makulera"
    Så finns inte något recept med innehåll:
      | Elvanse | Scenariotestfall_3-1 |
    Och antalet historiska recept är oförändrat
    Och jag loggar ut

  Scenario: 8-2 Makulera en aktuell HF recept
    Givet att det finns ett recept med innehåll:
      | Detrusitol | Scenariotestfall_3-10 |
    När jag väljer "Makulera" i menyn för receptet
    Och dialogtexten innehåller "Det finns en aktiv beställning för detta recept som kommer avvisas vid makulering."
    Och jag väljer makuleringsorsak "Annan orsak. Patienten informerad."
    Och jag anger makuleringsorsak "Scenariotestfall_8-2"
    Och jag klickar "Makulera"
    Så finns inte något recept med innehåll:
      | Detrusitol | Scenariotestfall_3-10 |
    Och antalet historiska recept har ökat med 1
    Och det finns minst ett historiskt recept som har slutdatum idag, med innehåll:
      | Detrusitol | Utsatt: Förlängning |
    # BUG! Får orsak = Förlägning (föregående orsak)
    Och jag loggar ut

  Scenario: 8-3 Makulera en handelsvaro recept
    När jag går till fliken "Handelsvaror"
    Och jag noterar antalet historiska recept
    Givet att det finns ett recept med innehåll:
      | Freedom Spruta | Scenariotestfall_3-14 |
    När jag väljer "Makulera" i menyn för receptet
    Och jag väljer makuleringsorsak "Fel patient"
    Och jag klickar "Makulera"
    Så finns inte något recept med innehåll:
      | Freedom Spruta | Scenariotestfall_3-14 |
    Och antalet historiska recept är oförändrat
    Och jag loggar ut
