#language: sv

Egenskap: 4-Ändra
  Ändringar av recept

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad

    Scenario: 4-1 Ändra ett dispenserad recept från 12 till 4 tillfällen
      Givet att det finns ett recept med innehåll:
        | Elvanse | 30 mg | Kapsel, hård |  08 1 + 09 1 + 10 1 + 11 1 + 12 1 + 13 1 + 15 1 + 16 1 + 17 1 + 18 1 + 19 1 + 20 1 | Scenariotestfall_3-1 |
      När jag väljer "Ändra" i menyn för receptet
      Så visas dialogen "Planerad utsättning"
      Och jag klickar "Stäng"
      Och jag klickar "Byt läkemedel"
      Och jag väljer "Elvanse" "Kapsel, hård" "50 mg" från lista
      Och jag klickar "Välj läkemedel"
      Så visas dialogen "Läkemedelsspecifikation ändrad"
      Och jag klickar "Fortsätt"
      Och jag anger ändringsorsak "Scenariotestfall_4-1"
      Och jag anger kortnotation "1x4"
      Och jag anger förmån "Nej"
      Och meddelande om villkor för förmån visas
      Och jag klickar "Ändra"
      Så finns ett framtidrecept med innehåll:
        | Elvanse | 50 mg | Kapsel, hård | 08 1 + 12 1 + 16 1 + 20 1 | Scenariotestfall_3-1 |
      Och receptet är markerad som ändrad
      Och jag loggar ut

    Scenario: 4-2 Ändra ett dispenserad recept med ny dos i framtiden
      Givet att det finns ett recept med innehåll:
        | Digoxin BioPhausia | 0,25 mg | Tablett | 08 D | oregelbunden | Scenariotestfall_3-4 |
      När jag väljer "Ändra" i menyn för receptet
      Och jag fyller i för alla dagar i doseringsschemat:
        | 10 |
        | 2  |
      Och jag anger ändringsorsak "Scenariotestfall_4-2"
      Och jag klickar "Ändra"
      Så finns ett framtidrecept med innehåll:
        | Digoxin BioPhausia | 0,25 mg | Tablett | 08 D | oregelbunden | Scenariotestfall_3-4 |
      Och receptet är markerad som ändrad
      Och receptet har en framtida ändring till:
        | Digoxin BioPhausia | 0,25 mg | Tablett | 10 D | oregelbunden | Scenariotestfall_3-4 |
      Och jag loggar ut

    Scenario: 4-3 Ändra disp recept till HF med nytt läkemedel
      Givet att det finns ett recept med innehåll:
      #Olika testpatienter får olika sortiment för Dispenserbara läkemdel
        | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      #  | Alvedon | 500 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      #  | Paracetamol NET | 500 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      När jag väljer "Ändra till helförpackning" i menyn för receptet
      Så visas dialogen "Planerad utsättning"
      Och jag klickar "Stäng"
      Och jag klickar "Byt läkemedel"
      Och jag markerar "Får ej bytas"
      Och jag väljer "Ipren" "Filmdragerad tablett" "400 mg" från lista
      # Och jag väljer "Paracut" "Tablett" "250 mg" från lista
      Och jag klickar "Välj läkemedel"
      Så visas dialogen "Läkemedelsspecifikation ändrad"
      Och jag klickar "Fortsätt"
      Och jag anger insättning 7 dagar efter första dag i dosperiod
      Och jag anger ändringsorsak "Scenariotestfall_4-3"
      Och jag klickar "Ändra"
      Så finns ett aktuellt recept med innehåll:
        | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      #  | Alvedon | 500 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      #  | Paracetamol NET | 500 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      Och receptet är markerad som ändrad
      Och receptet har en framtida ändring till:
        | Ipren | 400 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
    #  | Paracut | 250 mg | Tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
      Och jag loggar ut

    Scenario: 4-4 Ändra disp recept insättningsdatum flyttas till efter befintligt utsättningsdatum
      Givet att det finns ett recept med innehåll:
        | Tolterodin Sandoz | 1 mg | Filmdragerad tablett | 08 D + 16 D | oregelbunden | Scenariotestfall_3-2 |
      När jag väljer "Ändra" i menyn för receptet
      Så visas dialogen "Planerad utsättning"
      Och jag klickar "Stäng"
      Så visas dublettvarning
      Och jag anger ändringsorsak "Scenariotestfall_4-4"
      Och jag anger insättning 3 dagar efter befintligt utsättningsdatum
      Så visas en varning som innehåller texten "uppehåll i behandling på 2 dagar"
      Och jag klickar "Ändra"
      Så visas dialogen "Bekräfta dubbelförskrivning"
      Och jag klickar "Bekräfta"
      Så finns ett framtidrecept med innehåll:
        | Tolterodin Sandoz | 1 mg | Filmdragerad tablett | 08 D + 16 D | oregelbunden | Scenariotestfall_3-2 |
      Och receptet är markerad som ändrad
      Och receptet har en framtida ändring till:
        | Tolterodin Sandoz | 1 mg | Filmdragerad tablett | 08 D + 16 D | oregelbunden | Scenariotestfall_3-2 |
      Och jag loggar ut

    # Måste välja nytt lkm efter ändring till dospåse för vissa patienter. Dos sortimentsproblem.
    Scenario: 4-5 Ändra en helförpackning till dispenserad recept
      Givet att det finns ett recept med innehåll:
        | Oxascand | 15 mg | Tablett | 15 mg 3-4 gånger dagligen, som vid behov kan ökas till 30 mg 3-4 gånger dagligen. | Scenariotestfall_3-7 |
      När jag väljer "Ändra till dospåse" i menyn för receptet
      Så visas dialogen "Planerad utsättning"
      Och jag klickar "Stäng"
      Så ändringsorsak är förifylld "Till dospåse"
      Och jag anger kortnotation "1x2"
      Och jag klickar "Ändra"
      Så finns ett aktuellt recept med innehåll:
        | Oxascand | 15 mg | Tablett | 15 mg 3-4 gånger dagligen, som vid behov kan ökas till 30 mg 3-4 gånger dagligen. | Scenariotestfall_3-7 |
      Och receptet är markerad som ändrad
      Och receptet har en framtida ändring till:
        | Oxascand | 15 mg | Tablett | 08 1 + 20 1 | Scenariotestfall_3-7 |
      Och jag loggar ut
