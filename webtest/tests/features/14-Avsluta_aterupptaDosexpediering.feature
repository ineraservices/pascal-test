#language: sv

Egenskap: 14-Avsluta och återuppta dosexpediering
  Avsluta dosexpediering och sätt dospatientens status till Avliden.
  Ta bort status Avliden och återuppta dosexpediering.

  Scenario: 14-1 Avsluta dosexpediering
    Givet att jag är inloggad i Pascal som Sjuksköterska
    Och dos-patient är hämtad
    Och Patientinformation fliken är vald
    Och jag klickar "Avsluta patient"
    Så visas dialogen "Avsluta dosexpediering"
    Och jag klickar "Markera som avliden"
    Så visas dialogen "Avregistrera avliden dospatient"
    Givet jag väljer skriva ut Receptutskrift Pascal
    Och jag klickar tillbaka till Pascalsida
    Och jag klickar "Avsluta dosexpediering"
    Så visas dialogen "Avregistrera avliden dospatient"
    Och jag klickar "Avregistrera dospatient"
    Så visas dialogen "Avregistrering genomförd"
    Och jag klickar "Stäng dospatienten"
    #Och jag stänger patientsidan
    #Och på startsidan visar patienten status "Avliden"
    Och jag loggar ut

    Scenario: 14-2 Återuppta dosexpediering
      Givet att jag är inloggad i Pascal som Läkare
      Och dos-patient är hämtad
      Och patienten är markerad med status "Avliden"
      # Kolla teststeg 2
      Och Patientinformation fliken är vald
      Och jag klickar "Återuppta dosexpediering"
      Så visas dialogen "Återuppta dosexpediering"
      Och jag klickar "Återuppta expediering"
      Så visas dialogen "Återuppta dosexpediering"
      Och jag klickar "Återuppta dosexpediering" i dialogen
      Så visas dialogen "Dosexpediering återupptagen"
      Och jag klickar "Stäng"
      # Kolla teststeg 5
      Och jag loggar ut
    
    Scenario: 14-3 Pausa och Aktivera leverans av dosrullar för patienten
      Givet att jag är inloggad i Pascal som Sjuksköterska
      Och dos-patient är hämtad
      Och Patientinformation fliken är vald
      Och jag klickar "Ändra patientinformation och status"
      Och jag väljer expediering "Vilande"
      Och jag anger orsak "Inlagd"
      Och jag klickar "Spara och stäng"
      Och patienten är markerad med status "Vilande"
      Och jag stänger patientsidan
      # Ev. framtid funktion
      #Och på startsidan visar patienten status "Vilande"
      Och dos-patient är hämtad
      Och Patientinformation fliken är vald
      Och jag klickar "Ändra patientinformation och status"
      Och jag väljer expediering "Aktiv"
      Så visas dialogen "Begäran om akutproduktion"
      Och jag klickar "Begär akutproduktion"
      # Hantering av hemmaboende eller ej (Ordinär- eller Särskildboende)
      Och jag hanterar dialogen Informera patienten
      Och jag klickar "Spara och stäng"
      Och jag loggar ut
