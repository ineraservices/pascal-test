# Find element by "ref" attribute
Capybara.add_selector(:ref) do
  xpath { |ref| XPath.descendant[XPath.attr(:ref) == ref.to_s] }
end

# Find element by "placeholder" attribute
Capybara.add_selector(:placeholder) do
  xpath { |placeholder| XPath.descendant[XPath.attr(:placeholder) == placeholder.to_s] }
end

# Find following element
Capybara.add_selector(:next) do
  xpath { |type| "./following-sibling::#{type}[1]" }
end
