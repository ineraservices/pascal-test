require 'selenium-webdriver'

puts "============ Testparamtetrar ============"
puts " ENDPOINT: " + ENV['ENDPOINT']
puts " DOS_PATIENT_ID: " + ENV['DOS_PATIENT_ID']
puts " ICKE_DOS_PATIENT_ID: " + ENV['ICKE_DOS_PATIENT_ID']
puts "========================================="

Capybara.app_host = ENV['ENDPOINT'] # Should be changed in docker-compose.yml.
Capybara.default_driver = :selenium_remote_chrome
Capybara.javascript_driver = :selenium_remote_chrome
Capybara.register_driver "selenium_remote_chrome".to_sym do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    "chromeOptions" => {"args" => [ "window-size=1600,1000" ]}
  )
  Capybara::Selenium::Driver.new(
    app, browser: :remote, url: ENV['SELENIUM_SERVER'],
    desired_capabilities: capabilities)
end
# Setup for screenshots
Capybara.save_path = "output"
Capybara::Screenshot.register_driver(:selenium_remote_chrome) do |driver, path|
  driver.browser.save_screenshot(path)
end
# Wait time (seconds) may be adjusted or overridden for individual steps
Capybara.default_max_wait_time = 15
# Attempt to click the label to toggle state if element is non-visible (useful for styled checkboxes)
Capybara.automatic_label_click = true
# Old behaviour when matching text (simplifies checking table rows)
Capybara.default_normalize_ws = true

# After hooks

After('@abort_on_failure') do |s|
  # Tell Cucumber to quit after this scenario is done - if it failed.
  Cucumber.wants_to_quit = true if s.failed?
end
