#language: sv

Egenskap: 5-Förnya
  Förnya recept

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad

  Scenario: 5-1 Förnya ett HF recept
    Givet att det finns ett recept med innehåll:
      | Detrusitol | 2 mg | Filmdragerad tablett | 20 1 | Scenariotestfall_3-10 |
    När jag väljer "Förnya" i menyn för receptet
    Så visas dialogen "Förnya"
    Och visas dublettvarning
    Och jag lägger till förnyelsekommentar "Scenariotestfall_5-1"
    Och jag ändrar giltighetstid till 11 månader
    Och ikonen för äldrevarning visas
    Och ikonen för interaktionsvarning visas
    När jag klickar på interaktionsvarning
    Så visas dialogen "Varningsöversikt för Detrusitol"
    Och dialogtexten innehåller "mot Waran"
    Och jag klickar "Stäng"
    Och jag klickar "Förnya"
    Så visas dialogen "Bekräfta dubbelförskrivning(ar)"
    Och jag klickar "Bekräfta"
    Så finns ett aktuellt recept med innehåll:
      | Detrusitol | 2 mg | Filmdragerad tablett | 20 1 | Scenariotestfall_3-10 |
    Och jag loggar ut

  Scenario: 5-2 Förnya ett Extempore recept
    Givet att det finns ett recept med innehåll:
      | Extempore | Läkemedlets namn, form, styrka och total mängd | 08 D | oregelbunden | Scenariotestfall_3-8 |
    När jag väljer "Förnya" i menyn för receptet
    Så visas dialogen "Förnya"
    Och meddelande om ändra istället visas
    Och jag klickar på ändra istället
    Så visas dialogen "Planerad utsättning"
    Och jag klickar "Stäng"
    Så ändringsorsak är förifylld "Förnyelse"
    Och jag väljer doseringsenhet "dospåse"
    Och jag väljer antal uttag 1
    Och jag väljer antal fp 1
    Och jag klickar "Ändra"
    Så finns ett aktuellt recept med innehåll:
      | Extempore | Läkemedlets namn, form, styrka och total mängd | 08 D | oregelbunden | Scenariotestfall_3-8 |
    Och receptet är markerad som ändrad
    Och jag loggar ut

  Scenario: 5-3 Multi Förnya HV recept
    Givet Handelsvaror fliken är vald
    När jag markerar receptet med innehåll:
      | Teknisk Sprit | Scenariotestfall_3-11 |
    Och jag markerar receptet med innehåll:
      | Cutisoft Wipes | Scenariotestfall_3-12 |
    Och jag markerar receptet med innehåll:
      | Natriumklorid | Scenariotestfall_3-13 |
    Och jag klickar "Förnya"
    Så visas dialogen "Förnya"
    Och jag klickar "Förnya" i dialogen
    Och jag loggar ut
