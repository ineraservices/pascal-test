#language: sv

Egenskap: 15-Roller och behörigheter
  Behörighetskontroll till olika tjänster med olika roller

  Scenario: 15-1 Inloggad som sjuksköterska med förskrivningsrätt
    Givet att jag är inloggad i Pascal som Sjuksköterska
    Och icke-dos-patient är hämtad
    När jag klickar på länken registrera samtycke
    Så visas dialogen "Registrera samtycke till läkemedelsförteckningen"
    Och jag klickar "Välj engångssamtycke"
    Och jag stänger patientsidan
    Och dos-patient är hämtad
    Och jag klickar "Ny förskrivning"
    Givet jag väljer helförpackning
    När jag söker efter läkemedel "Waran"
    Så meddelande om begränsad förskrivningsrtätt visas
    Och jag stänger receptsdialogen
    Och jag loggar ut

  Scenario: 15-2 Inloggad som sjuksköterska utan förskrivningsrätt
    Givet att jag är inloggad i Pascal som Sjuksköterska utan förskrivningsrätt
    Och icke-dos-patient är hämtad
    När jag klickar på länken registrera samtycke
    Så visas dialogen "Registrera samtycke till läkemedelsförteckningen"
    Och jag klickar "Välj nödåtkomst"
    Och jag stänger patientsidan
    Och dos-patient är hämtad
    Så visas inte Ny förskrivning knappen
    # samt andra knappar förutom Beställning ska inte visas heller
    Och jag loggar ut
  @wip
  Scenario: 15-3 Inloggad som Ej legitimerad vårdpersonal
    Givet att jag är inloggad i Pascal som Ej legitimerad vårdpersonal
    Och dos-patient är hämtad
    # Några teststeg ska läggas till
    Och jag loggar ut
