När(/^jag makulerar alla framtida recept$/) do
  wait_for_loading()
  find('#Patient').all("tbody.gray.section-body input[type='checkbox']").each do |checkbox|
    if not checkbox.checked?
      checkbox.find(:xpath, './../label').click()
    end
  end
  if page.has_button?('Makulera', disabled: false)
    within('div.tableWrapper') do
      click_button('Makulera')
    end
    hanteraMakuleraDialog('Fel patient', 'Fel läkemedel/vara/styrka/dosering/ändamål. Patienten informerad.')
  end
end

När(/^jag makulerar alla makulerbara recept$/) do
  wait_for_loading()
  find('#Patient').all("tbody.section-body input[type='checkbox']").each do |checkbox|
    if not checkbox.checked?
      checkbox.find(:xpath, './../label').click()
    end
  end
  if page.has_button?('Makulera', disabled: false)
    within('div.tableWrapper') do
      click_button('Makulera')
    end
    hanteraMakuleraDialog('Fel handelsvara/vara/styrka/dosering/ändamål. Patienten informerad.')
  end
end

När(/^jag makulerar alla makulerbara recept ett i taget$/) do
  wait_for_loading()
  rowXpath = ".//tbody[contains(@class, 'section-body')]/tr[descendant::input]"
  while !find('#Patient').has_no_selector?(:xpath, rowXpath, wait: 15) #Waiting selector
    makulerbar = false
    find('#Patient').all(:xpath, rowXpath).each do |rad|
      rad.find('i.fa-bars').click()
      expect(page).to have_selector(:ref, 'tableMenu')
      makulerbar = find(:ref, 'tableMenu').has_content?('Makulera', wait: 5)
      if makulerbar
        within(:ref, 'tableMenu') do
          click_on('Makulera')
        end
        hanteraMakuleraDialog('Fel patient', 'Fel läkemedel/vara/styrka/dosering/ändamål. Patienten informerad.')
        break
      else
        rad.find('i.fa-bars').click()
      end
    end
    break if !makulerbar
  end
end

När(/^jag sätter ut alla återstående recept$/) do
  wait_for_loading()
  find('#Patient').all("tbody.section-body input[type='checkbox']").each do |checkbox|
    if not checkbox.checked?
      checkbox.find(:xpath, './../label').click()
    end
  end
  if page.has_button?('Sätt ut', disabled: false)
    within('div.tableWrapper') do
      click_button('Sätt ut')
    end
    expect(page).to have_selector('h1', text: 'Sätt ut')
    within('div.pl-modal') do
      if page.has_selector?('label', text: 'Gemensamt utsättningsdatum och orsak')
        check('Gemensamt utsättningsdatum och orsak')
        expect(page).to have_selector('div.prescriptionActionCommonOption')
      end
      select('Planerad utsättning')
      check('Sätt ut direkt')
    end
    sleep(3)
    if page.has_content?('Utsättning innan sista dosdag')
      click_on('Stäng')
    end
    within('div.pl-modal') do
      click_button('Sätt ut')
    end
  end
end

När /^jag avbeställer alla aktiva beställningar/ do
  wait_for_loading()
  find('#Orders').all("tbody.section-body input[type='checkbox']", wait: 5).each do |checkbox|
    if not checkbox.checked?
      checkbox.find(:xpath, './../label').click()
    end
  end
  if page.has_button?('Avbeställ', disabled: false, wait: 5)
    click_button('Avbeställ')
    expect(page).to have_selector('h1', text: 'Avbeställ')
    within('div.pl-modal') do
      click_button('Avbeställ')
    end
  end
end

Så /^finns inga aktuella eller framtida recept$/ do
  wait_for_loading()
  expect(page).to have_no_selector('#Patient tbody.section-header', text: 'Aktuella')
  expect(page).to have_no_selector('#Patient tbody.section-header', text: 'Framtida')
end

Så /^är alla beställningar avbeställda$/ do
  expect(page).to have_no_selector('#Orders #Patient tbody.section-body', text: 'Mottagen')
end

def hanteraMakuleraDialog(*orsaker)
  expect(page).to have_selector('div.pl-modal h1', text: 'Makulera')
  within('div.pl-modal') do
    page.all('div.actionItem').each do |item|
      if item.all('notification-block.error', wait: 2).any?
        item.find('button').click()
      end
    end
    orsaker.each do |orsak|
      if page.has_selector?('select option', text: orsak, wait: 2)
        select(orsak)
        break
      end
    end
    click_button('Makulera')
    sleep(5)
  end
end

Så /^jag rensar förberedd data$/ do
  click_on("Förbered som dospatient")
  if page.has_selector?('button.pl-button.left', text: 'Ta bort förberedd data')
    click_on("Ta bort förberedd data")
    click_on("Ta bort")
    click_on("Stäng")
  else
    click_on("Avbryt")
  end
end

Och /^jag rensar bevakningslistan$/ do
  within('div#Header') do
   find('li.menuItem.settings').click()
 end
 find('div.description', text: 'Bevakning').click()
 if !page.has_selector?('table.pl-list.small.userMonitoredItemList')
   click_on("Avbryt")
 else
   while page.has_selector?('table.pl-list.small.userMonitoredItemList') do
     first('table.pl-list.small.userMonitoredItemList button').click()
   end
   click_on("Spara & stäng")
 end
end

Givet /^Pascal föreslår förpackning och antalet uttag för helförpackningsrecept är vald$/ do
  within('div#Header') do
   find('li.menuItem.settings').click()
 end
 find('div.description', text: 'Förskrivning').click()
 check('SuggestDispenseAuthorizationParameters')
 click_on("Spara & stäng")
end

Och /^jag väntar på att sidan laddas upp$/ do
  wait_for_loading()
end
