### Sökning efter recept i listan ###

Givet /^att det finns ett recept med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  wait_for_loading()
  @recept = find('#Patient table.pl-table tbody.section-body tr', text: re)
end

Givet /^att det finns minst ett historiskt recept med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  wait_for_loading()
  expect(find('#Patient table.pl-table')).to have_selector(:xpath, ".//tbody[contains(@class, 'section-body')]/tr[not(descendant::input)]", text: re, minimum: 1)
  @recept = first('#Patient table.pl-table').first(:xpath, ".//tbody[contains(@class, 'section-body')]/tr[not(descendant::input)]", text: re)
end

Så /^finns inte något recept med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  wait_for_loading()
  expect(find('#Patient table.pl-table')).to have_no_selector(:xpath, "//tbody/tr[descendant::input]", text: re)
end

Så /^finns ett framtidrecept med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  wait_for_loading()
  @recept = find('#Patient table.pl-table').find(:xpath, ".//tbody[contains(@class, 'section-body') and contains(@class, 'gray')]/tr[descendant::input]", text: re)
end

Så /^finns ett aktuellt recept med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  wait_for_loading()
  @recept = find('#Patient table.pl-table').find(:xpath, ".//tbody[contains(@class, 'section-body') and not(contains(@class, 'gray'))]/tr[descendant::input]", text: re)
end

Och /^det finns minst ett historiskt recept som har slutdatum idag, med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+') + '.+' + Date.today.to_s)
  wait_for_loading()
  expect(find('#Patient table.pl-table')).to have_selector(:xpath, ".//tbody[contains(@class, 'section-body')]/tr[not(descendant::input)]", text: re, minimum: 1)
end

Och /^jag sparar undan vald läkemdel$/ do
lkmrad = find('tr.au-target.selected').text
puts lkmrad
end

### Ytterligare kontroller kopplade till sökt recept (måste föregås av något av ovanstående steg) ###

Och  /^receptet är markerad som ny$/ do
  expect(@recept).to have_selector('span.desc', text: 'Ny')
end

Och  /^receptet är markerad som ändrad$/ do
  expect(@recept).to have_selector('span.desc', text: 'Ä', wait: 5)
end

Och /^receptet har EXT-symbolen$/ do
    expect(@recept).to have_selector('span.desc', text: 'EXT')
end

Och /^receptet har LIC-symbolen$/ do
    expect(@recept).to have_selector('span.desc', text: 'LIC')
end

Och /^receptets utsättningsdatum är idag$/ do
  within(@recept) do
    utsattning = all('td')[10]
    expect(utsattning).to have_content(Date.today.to_s)
  end
end

Och /^receptets utsättningsdatum är om (\d+) dagar$/ do |dagar|
  within(@recept) do
    utsattning = all('td')[10]
    datum = Date.today.next_day(dagar.to_i).to_s
    expect(utsattning).to have_content(datum)
  end
end

Och /^receptet har en framtida ändring till:$/ do |table|
  data = table.raw[0]
  andring = @recept.find(:next, 'tr')
  expect(andring).to have_selector('i.fa-arrow-right')
  data.each { |cell|
    expect(andring).to have_content(cell)
  }
end

### Kontroll av antal historiska ###

Givet(/^jag noterar antalet historiska recept$/) do
  @historiska = '0'
  if page.has_selector?('tbody.gray.section-header span', text: 'Historiska')
    headertext = find('tbody.gray.section-header span', text: 'Historiska').text
    @historiska = headertext.scan(/\d+/).first
  end
  puts @historiska
end

Så(/^antalet historiska recept är oförändrat$/) do
  if not @historiska = '0'
    sleep(5) # Vänta så att eventuell (felaktig) uppdatering hinner ske
    expect(find('tbody.gray.section-header span', text: 'Historiska')).to have_content("(#{@historiska})")
  end
end

Så(/^antalet historiska recept har ökat med (\d+)$/) do |nya|
  expected = @historiska.to_i + nya.to_i
  expect(find('tbody.gray.section-header span', text: 'Historiska')).to have_content("(#{expected})")
end

### Använd menyn för sökt recept(måste föregås av sökning efter recept) ####

När /^jag väljer "([^"]*)" i menyn för receptet$/ do |menyval|
  within(@recept) do
    find('i.fa-bars').click()
  end
  expect(page).to have_selector(:ref, 'tableMenu')
  within(:ref, 'tableMenu') do
    click_on(menyval)
  end
end

### Markering för att använda multi-funktioner ###

När /^jag markerar receptet med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  rad = find('#Patient table.pl-table').find(:xpath, ".//tbody[contains(@class, 'section-body')]/tr[descendant::input]", text: re)
  checkbox = rad.find("input[type='checkbox']")
  if not checkbox.checked?
    checkbox.find(:xpath, './../label').click()
  end
end

### Varningar ###

Och /^dubblettvarning är inte aktiv$/ do
  expect(page).to have_no_selector('doublemedication-button', text: 'st')
end

Och /^äldrevarning är aktiv$/ do
  expect(page).to have_selector('notrecommendedforelderly-button', text: 'st')
end

### Makluering / Utsättining ###

Och /^jag väljer makuleringsorsak "([^"]*)"$/ do |orsak|
  within('div.pl-modal') do
    select(orsak)
  end
end

Och /^jag anger makuleringsorsak "([^"]*)"$/ do |fritext|
  within(find('div.inlineBlock.spaceBefore', text: 'Annan orsak')) do
    find('input').set(fritext)
  end
end

Och /^jag anger utsättningsorsak "([^"]*)"$/ do |orsak|
  within('div.pl-modal') do
    select(orsak)
  end
end

Och /^jag anger utsättningsorsak i fritext "([^"]*)"$/ do |fritext|
  within('div.pl-modal div.form-group', text: 'Ange utsättningsorsak i fritext*') do
    find('input').set(fritext)
  end
end

Och /^jag anger utsättningsdatum idag/ do
  dateBox = find('div.pl-modal input[id^="EndPrescriptionDatePicker_"]')
  dateBox.set(Date.today.to_s)
  dateBox.native.send_keys(:tab)
end

Och /^jag anger utsättningsdatum om (\d+) dagar$/ do |dagar|
  dateBox = find('div.pl-modal input[id^="EndPrescriptionDatePicker_"]')
  dateBox.set(Date.today.next_day(dagar.to_i).to_s)
  dateBox.native.send_keys(:tab)
end

Och /^utsättningstid är "([^"]*)"$/ do |tid|
  within(find('div.pl-modal div.inlineBlock', text: 'Utsättningstid')) do
    expect(page).to have_content(tid)
  end
end

Och /^utsättningstid är aktuell tid$/ do
  within(find('div.pl-modal div.inlineBlock', text: 'Utsättningstid')) do
    tid = DateTime.now.strftime("%H:") # Matchar endast timme, minut kan lätt bli fel
    expect(page).to have_content(tid)
  end
end

 ### Skriva ut Läkemedelslista Dos/Receptutskrift Pascal ###

 Givet /^jag väljer skriva ut Receptutskrift Pascal/ do
   find("i.fa.fa-print").click()
 end

 Och /^jag klickar tillbaka till Pascalsida/ do
   page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
 end

 def wait_for_loading()
   sleep(3)
   expect(page).to have_no_content('Laddar...', wait: 30)
 end
