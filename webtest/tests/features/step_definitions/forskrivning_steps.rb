### Dospåse/helförpackning ###

Givet /^(?:att )?jag väljer helförpackning$/ do
  find('div.tab', text: 'Helförpackning').click()
end

### Sök/välj läkemedel ###

Givet /^(?:att )?jag väljer läkemedel "([^"]*)"$/ do |term|
  find(:ref, 'searchInputElement').set(term)
  click_button('Välj läkemedel')
end

Givet /^(?:att )?jag söker efter läkemedel "([^"]*)"$/ do |term|
  find(:ref, 'searchInputElement').set(term)
end

Och /^jag rensar sökfältet$/ do
  find('div.float-input-clear-wrapper span').click()
end

Och /^jag väljer "([^"]*)" "([^"]*)" "([^"]*)" från lista$/ do |lkm, form, styrka|
  re =  Regexp.new(Regexp.escape(lkm) + '.+' + Regexp.escape(form) + '.+' + Regexp.escape(styrka))
  find('table.drugResultsList tr', text: re).find('td', text: lkm).click()
end

### välj icke-godkända läkemedel ###

Och /^jag klickar länken icke-godkända läkemedel$/ do
  find('div.alsoSearchSpecialArticle', text: 'icke-godkända läkemedel').click()
end

Och /^jag anger läkemedelsspecifikation "([^"]*)"$/ do |specifikation|
  find(:placeholder, 'Specifikation extemporeförskrivning').set(specifikation)
end
### Gemensamma data ###

Och /^jag anger ändamål "([^"]*)"$/ do |andamal|
  fill_in('PrescriptionReason', :with => andamal)
end

Och /^jag anger ändringsorsak "([^"]*)"$/ do |orsak|
  fill_in('Ändringsorsak', :with => orsak)
end

### Dosering ###

När /^jag anger kortnotation "([^"]*)"$/ do |kortnotation|
  sleep (5)
  fill_in('ShortNotationInput', :with => kortnotation)
  sleep (5)
end

Så /^visas kortnotation "([^"]*)"$/ do |kortnotation|
  expect(page).to have_field('ShortNotationInput', with: kortnotation)
end

Och /^jag väljer oregelbunden dosering med schema "([^"]*)"$/ do |schema|
  find('tab', text: 'Oregelbunden dosering').click()
  expect(find('div.pl-modal')).to have_content('Doseringsschema')
  select(schema)
end

Och /^jag väljer när schemat nått sista dagen, ska schemat "([^"]*)"$/ do |upprepning|
  select(upprepning)
end

Och /^jag väljer antal dagar i schema "([^"]*)"$/ do |dagar|
  select(dagar)
end

Och /^jag fyller i för alla dagar i schemat för annan orgelbunden dosering:$/ do |schema|
  page.all('div.dosageScheduleDay').each do |day|
    i=0
    tider = schema.raw[0]
    doser = schema.raw[1]
    day.all('div.dosageScheduleOccasion').each do | occasion |
      within(occasion) do
        #select(tider[i])
        find('input.quantity').set(doser[i])
        i = i + 1
      end
    end
  end
end

Och /^jag fyller i för dag (\d+) till (\d+) i schemat för annan orgelbunden dosering:$/ do |start, slut, schema|
  days = Range.new(start, slut)
  days.each do |day|
    i=0
    tider = schema.raw[0]
    doser = schema.raw[1]
    first('div.dosageScheduleDayWrapper', text: "Dag #{day}").all('div.dosageScheduleOccasion').each do | occasion |
      within(occasion) do
        #select(tider[i])
        find('input.quantity').set(doser[i])
        i = i + 1
      end
    end
  end
end

Och /^jag fyller i för alla dagar i doseringsschemat:$/ do |schema|
  expect(page).to have_selector('div.dosageScheduleNamedDay')
  page.all('div.dosageScheduleNamedDay').each do |day|
    i=0
    tider = schema.raw[0]
    doser = schema.raw[1]
    day.all('div.dosageScheduleOccasion').each do | occasion |
      within(occasion) do
        select(tider[i])
        find('input.quantity').set(doser[i])
        i = i + 1
      end
    end
  end
end

Och /^jag anger dos "([^"]*)" alla dagar$/ do |dos|
  find('div.dosageScheduleOccasion input[type="text"]').set(dos)
end

Och /^jag anger dosering i fritext "([^"]*)"$/ do |fritext|
  find('tab', text: 'Dosering i fritext').click()
  find('textarea.freeText').set(fritext)
end

Och /^jag väljer doseringsenhet "([^"]*)"$/ do |doseringsenhet|
  within('div.dosageUnitWrapper') do
    select(doseringsenhet)
  end
end

Så /^visas information om doseringen "([^"]*)"$/ do |info|
    expect(page).to have_selector('div.irregularDosageTextContainer', text: info)
end

### Akut produktion ###

Och /^jag väljer akut produktion$/ do
  find('label', text: 'Akut produktion').click()
  if page.has_selector?('h1', text: 'Informera patienten!', wait: 5)
    click_on('Stäng')
  end
end

Och /^jag accepterar akut produktion om det krävs för valt datum$/ do
  if page.has_selector?('h1', text: 'Akutproduktion', wait: 5)
    click_on('Akutproduktion')
  end
  if page.has_selector?('h1', text: 'Informera patienten!', wait: 5)
    click_on('Stäng')
  end
end

### Datum ###

Och /^jag anger insättning (\d+) dagar innan första dag i dosperiod$/ do |dagar|
  dateBox =  find('input#StartDate')
  startDatumString = dateBox.value()
  insattningsDatum = Date.parse(startDatumString).prev_day(dagar.to_i)
  dateBox.set(insattningsDatum)
  dateBox.native.send_keys(:return)
end

Och /^jag anger insättning (\d+) dagar efter första dag i dosperiod$/ do |dagar|
  dateBox =  find('input#StartDate')
  startDatumString = dateBox.value()
  insattningsDatum = Date.parse(startDatumString).next_day(dagar.to_i)
  dateBox.set(insattningsDatum)
  dateBox.native.send_keys(:return)
end

Och /^jag anger insättning (\d+) dagar efter dagens datum$/ do |dagar|
  dateBox =  find('input#StartDate')
  insattningsDatum = Date.today.next_day(dagar.to_i)
  dateBox.set(insattningsDatum)
  dateBox.native.send_keys(:return)
end

Och /^jag anger utsättning efter (\d+) (.+)$/ do |period,tidsenhet|
  check('Sätts ut efter (utsättningsdatum)')
  within('div.endDateWrapper') do
    find(:xpath, './/input[@type="number"][not(@disabled)]', wait: 5).set(period)
    select(tidsenhet)
  end
end

Och /^jag anger insättning (\d+) dagar efter befintligt utsättningsdatum$/ do |dagar|
  utsattningsDatum = find('#PeriodLimitDate').value()
  insattningsDatum = Date.parse(utsattningsDatum).next_day(dagar.to_i)
  dateBox =  find('input#StartDate')
  dateBox.set(insattningsDatum)
  dateBox.native.send_keys(:return)
end

### Kommentarer ###

Och /^jag lägger till kommentar till vårdpersonal "([^"]*)"$/ do |kommentar|
  if page.has_no_link?('Lägg till kommentar till vårdpersonal')
    find('expando-header', text: 'Kommentar').click()
  end
  find('a', text: 'Lägg till kommentar till vårdpersonal').click()
  find(:placeholder, 'Kommentar till vårdpersonal').set(kommentar)
end

### Expediering ###

Och /^jag anger förmån "([^"]*)"$/ do |forman|
  select(forman)
end

Och /^meddelande om villkor för förmån visas$/ do
  expect(page).to have_selector('notification-block', text: 'Du har angett att patienten inte uppfyller villkoren för förmån. Det innebär att patienten inte kommer att få läkemedlet med förmån och får stå för hela kostnaden själv.')
end

Så(/^jag anger giltighetstid (\d+) månader$/) do |giltighetstid|
  check('LimitedValidity')
  fill_in('LimitedValidityEndPicker', :with => giltighetstid )
  within('div.dispenseAuthorizationParametersContainer') do
    select('månader')
  end
end

Och(/^jag anger receptetsgiltighetstid (\d+) dagar$/) do |tid|
  check('LimitedValidity')
  fill_in('LimitedValidityEndPicker', :with => tid )
  within(find('div.dispenseAuthorizationParameterGroup', text: 'Begränsad giltighetstid')) do
    select('dagar')
  end
end

Och /^jag väljer förpackning$/ do
  find("#SearchArticleInput").click()
  find("div.articleFilterResults").first('tr button').click()
end

När(/^jag väljer förpackning "([^"]*)" "([^"]*)"$/) do |namn, storlek|
  re =  Regexp.new(Regexp.escape(namn) + '.+' + Regexp.escape(storlek))
  find("#SearchArticleInput").click()
  rad = find("div.articleFilterResults tr", text: re)
  rad.find('button').click()
end

Och /^jag väljer antal uttag (\d+)$/ do |antal_uttag|
  within(find('div.dispenseAuthorizationParameterGroup', text: 'Antal uttag')) do
      first('input[type="number"]').set(antal_uttag)
  end
end

Och /^jag väljer antal fp (\d+)$/ do |antal_fp|
  within(find('div.dispenseAuthorizationParameterGroup', text: 'Antal fp/uttag')) do
      find('input[type="number"]').set(antal_fp)
  end
end

Och /^jag väljer Startförpackning$/ do
    click_on('Visa fler val')
    check('Startförpackning')
end

Och /^jag anger expeditionsinterval (\d+) (.+)$/ do |period,tidsenhet|
  find('#ExpeditionIntervalQuantity').set(period)
  find('#ExpeditionIntervalType').select(tidsenhet)
end

Så /^föreslår Pascal (\d+) uttag (\d+) fp$/ do |uttag,fp|
  expect(page).to have_checked_field('autoSelectDrugArticleChoice')
  within(find('div.dispenseAuthorizationParameterGroup', text: 'Antal uttag')) do
    expect(find('input').value.to_i).to eq(uttag)
  end
  within(find('div.dispenseAuthorizationParameterGroup', text: 'Antal fp/uttag')) do
    expect(find('input').value.to_i).to eq(fp)
  end
end

### Beslutsstöd ###

Så /^visas äldrevarning$/ do
  expect(page).to have_content('Undvik till äldre')
end

Så /^visas dublettvarning$/ do
  expect(page).to have_selector('div.duplicatesWrapper', text:'Dubblettvarning, följande läkemedel finns redan förskrivna')
end

Så /^visas en varning som innehåller texten "([^"]*)"$/ do |varning|
  expect(page).to have_selector('div.warning', text: varning)
end

Så /^ändringsorsak är förifylld "([^"]*)"$/ do |text|
  within('div.selectReasonWrapper', text:'Ändringsorsak') do
    expect(find('input').value).to eq(text)
  end
end

 ### Handelsvaror ###

 Och /^Handelsvaror fliken är vald/ do
   find('tab', text: 'Handelsvaror').click()
 end

 När /^(?:att )?jag söker efter handelsvara "([^"]*)"$/ do |term|
   find(:ref, 'searchInputElement').set(term)
 end

 Och /^jag väljer handelsvara "([^"]*)" "([^"]*)" "([^"]*)" från lista$/ do |hv, artnr, namn|
   re =  Regexp.new(Regexp.escape(hv) + '.+' + Regexp.escape(artnr) + '.+' + Regexp.escape(namn))
   find('table.articleResultsList tr', text: re).find('td', text: hv).click()
 end

 Och /^jag anger handelsvara ändamål "([^"]*)"$/ do |hvandamal|
  find('div.selectReasonWrapper input').set(hvandamal)
 end

 Och /^jag anger uttag (\d+)$/ do |hv_uttag|
  fill_in('1_expeditions', :with => hv_uttag)
 end

 Och /^jag anger förpackningar (\d+)$/ do |fp|
  fill_in('1_packages', :with => fp)
 end

 Och /^jag anger patientinstruktioner "([^"]*)"$/ do |patientinstruktioner|
   fill_in('Patientinstruktioner', :with => patientinstruktioner)
 end

 ### Förnya ###

 Så(/^jag lägger till förnyelsekommentar "([^"]*)"$/) do |kommentar|
   find('a', text: 'Lägg till kommentar').click()
   within(find('div.inlineBlock', text: 'Kommentar till vårdpersonal')) do
     find('input').set(kommentar)
   end
 end

 Så(/^jag ändrar giltighetstid till (\d+) (.+)$/) do |period,tidsenhet|
   check('Begränsad')
   find('input#undefined').set(period)
   select(tidsenhet)
 end

 Så(/^ikonen för äldrevarning visas$/) do
   expect(page).to have_selector('div.pl-modal i.pl-icon-elderly')
 end

 Så(/^ikonen för interaktionsvarning visas$/) do
   expect(page).to have_selector('div.pl-modal i.pl-icon-interaction1')
 end

 När(/^jag klickar på interaktionsvarning$/) do
   find('div.pl-modal i.pl-icon-interaction1').click()
 end

 Och /^meddelande om ändra istället visas$/ do
   expect(page).to have_selector('notification-block', text: 'Detta recept går ej att förlänga. Ändra receptet istället.')
 end

 Och /^jag klickar på ändra istället$/ do
   find('a', text: 'Gå till ändra recept').click()
 end
