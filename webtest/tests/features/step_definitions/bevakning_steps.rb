### Bevakaning av dos-patient ###

Och /^dospatienten rensas från bevakning$/ do
  within('div#Header') do
   find('li.menuItem.settings').click()
 end
 find('div.description', text: 'Bevakning').click()
 find('table.pl-list.small.userMonitoredItemList').
 find('tr', text: "#{ENV['DOS_PATIENT_ID']}").find('button').click()
 click_on("Spara & stäng")
end

Givet /^jag ändrar intställningar för bevakning med "([^"]*)" veckor$/ do |veckor|
  within('div#Header') do
   find('li.menuItem.settings').click()
 end
 find('div.description', text: 'Bevakning').click()
 select(veckor)
end

Och /^jag väljer att visa bevakade händelser "([^"]*)"$/ do |handelse|
 select(handelse)
end

Och /^jag väljer hantering av patienter "([^"]*)"$/ do |hantering|
 select(hantering)
end

Så /^information om utgående och utgångna recept visas med (\d+) dagar$/ do |dagar|
  tomDatum = Date.today.next_day(dagar.to_i)
  find('tab', text: 'Bevakade recept').click()
  expect(page).to have_selector('notification-block', text:
  "Visar utgående recept t.o.m. #{tomDatum} och utgångna/slutexpedierade recept 3 veckor bakåt i tiden")
end

Och /^bevakade händelser är tom/ do
  find('tab', text: 'Bevakade händelser (0)')
end

Och /^bevakade recept är tom/ do
  find('tab', text: 'Bevakade recept (0)')
end

Och /^Bevakningsfunktionen är aktiv/ do
  expect(page).to have_selector('notification-block.au-target.pl-notification.addfavorite')
end

Och /^Bevakningsfunktionen är inaktiv/ do
  expect(page).to have_no_selector('notification-block.au-target.pl-notification.addfavorite')
end

Och /^dospatienten visas under utgående recept/ do
  find('tab', text: 'Bevakade recept (1)').click()
  find('table.pl-list.small.prescriptionEndingList').
  find('tr', text: "#{ENV['DOS_PATIENT_ID']}")
end

Och /^dospatienten visas under bevakade händelser/ do
  find('tab', text: 'Bevakade händelser').click()
  find('table.pl-list.small.prescriptionChangesList').
  find('tr', text: "#{ENV['DOS_PATIENT_ID']}")
end
