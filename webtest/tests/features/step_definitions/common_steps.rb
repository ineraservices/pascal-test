Givet(/^att jag är inloggad i Pascal som (.*)$/) do |roll|
  visit('/')
  expect(page).to have_content('Pascal IDP', wait: 20)
  if roll == 'Läkare'
    find('div.choose-user', text: 'Daniella Jats').click()
  end
  if roll == 'Sjuksköterska'
    find('div.choose-user', text: 'Gunnar Perez').click()
    expect(page).to have_content('Välj aktuell roll', wait: 20)
    select(roll)
    click_button('Fortsätt')
  end
  if roll == 'Sjuksköterska utan förskrivningsrätt'
    find('div.choose-user', text: 'Mia Rickardsson').click()
  end
  if roll == 'Administratör'
    find('div.choose-user', text: 'Andrea Ulflund').click()
  end
  expect(page).to have_selector('h1', text: 'Snabbstartguide')
  find('label').click()
  click_on('Stäng')
  click_on('Avstå')
end

Givet(/^Gamla IDPn att jag är inloggad i Pascal som (.*)$/) do |roll|
  visit('/')
  expect(page).to have_content('Välj aktuell roll', wait: 20)
  select(roll)
  click_button('Fortsätt')
  expect(page).to have_selector('div#Start', wait: 45)
end

Och /^patient "([^"]*)" är hämtad/ do |personnummer|
  if not find('table#PatientsTable').has_content?(personnummer)
    find(:ref, 'searchPatientInput').set(personnummer)
  end
  within('table#PatientsTable') do
    expect(page).to have_content(personnummer)
    click_on(personnummer)
  end
  wait_for_loading()
  within('div#Header') do
    expect(page).to have_content(personnummer)
  end
end

Och /^dos-patient är hämtad/ do
  steps %{
    Och patient "#{ENV['DOS_PATIENT_ID']}" är hämtad
  }
end

Och /^icke-dos-patient är hämtad/ do
  steps %{
    Och patient "#{ENV['ICKE_DOS_PATIENT_ID']}" är hämtad
  }
end

Och /^jag går till fliken "([^"]*)"/ do |flik|
  find('tab', text: flik).click()
end

När /^jag klickar "([^"]*)"$/ do |knapptext|
  click_on(knapptext)
  wait_for_loading()
end

När /^jag klickar "([^"]*)" i dialogen$/ do |knapptext|
  if(page.all('ux-dialog').any?)
    within('ux-dialog') do
      click_on(knapptext)
    end
  else
    within('div.pl-modal') do
      click_on(knapptext)
    end
  end
  wait_for_loading()
end

När /^jag markerar "([^"]*)"$/ do |checkbox_label|
  check(checkbox_label)
end

Så(/^(?:är )?"([^"]*)" (?:är )?markerad$/) do |checkbox_label|
  expect(page).to have_checked_field(checkbox_label)
end

Så(/^(?:är )?"([^"]*)" (?:är )?inte markerad$/) do |checkbox_label|
  expect(page).to have_unchecked_field(checkbox_label)
end

Så /^visas dialogen "([^"]*)"$/ do |rubrik|
  expect(page).to have_selector('h1', text: rubrik)
end

Så(/^dialogtexten innehåller "([^"]*)"$/) do |text|
  if(page.all('ux-dialog').any?)
    expect(page).to have_selector('ux-dialog-body', text: text)
  else
    expect(page).to have_selector('div.pl-modal-content', text: text)
  end
end

Så(/^dialogtexten för dosaktivering visas "([^"]*)"$/) do |text|
  if(page.all('ux-dialog').any?)
    expect(page).to have_selector('ux-dialog-header', text: text)
  else
    expect(page).to have_selector('div.pl-modal-content', text: text)
  end
end

Så /^visas valideringsmeddelande "([^"]*)"$/ do |meddelande|
  expect(page).to have_selector('div.pl-popover-content', text: meddelande)
end

Och /^meddelande om en aktiv beställning visas$/ do
  expect(page).to have_selector('notification-block', text: 'aktiv beställning')
end

Och /^jag loggar ut$/ do
  within('div#Header') do
    find('li.logout').click()
  end
  expect(page).to have_content('Du är nu utloggad ur Pascal')
end

Och /^jag stänger receptsdialogen$/ do
   find('div.pl-modal-header button').click()
end

Och /^jag stänger patientsidan$/ do
  find('div.pl-buttonClose.large.au-target').click()
end

Så /^visas inte Ny förskrivning knappen/ do
  expect(page).to have_no_selector('newPrescriptionButton')
end

### Patientinformation ###

Och /^Patientinformation fliken är vald/ do
  find('tab', text: 'Patientinformation').click()
end

Och  /^patienten är markerad med status "([^"]*)"$/ do |status|
  expect(page).to have_selector('div.au-target.patientStatus', text: status)
end

Och /^på startsidan visar patienten status "([^"]*)"$/ do |status|
  expect(find('tr', text: "#{ENV['DOS_PATIENT_ID']}")).to have_content(status)
end

### Meddelande ###

Och /^Meddelande fliken är vald/ do
  find('tab', text: 'Meddelanden').click()
end

Och /^jag anger ärende "([^"]*)"$/ do |fritext|
  fill_in('Ärende', :with => fritext)
end

Och /^jag skriver meddelande "([^"]*)"$/ do |fritext|
  fill_in('Meddelande', :with => fritext)
end

### Testinfo ###

Och(/^jag öppnar information om Pascal$/) do
  find('div.footer span', text: 'Om Pascal').click
  expect(page).to have_selector('div.plv-aboutPascal')
end

Så(/^visas applikationens version$/) do
  version = first('div.plv-aboutPascal h1')
  puts version.text
end

Så(/^jag skriver ut testvariabler$/) do
  puts 'ENDPOINT: ' + ENV['ENDPOINT']
  puts 'DOS_PATIENT_ID: ' + ENV['DOS_PATIENT_ID']
  puts 'ICKE_DOS_PATIENT_ID: ' + ENV['ICKE_DOS_PATIENT_ID']
end
