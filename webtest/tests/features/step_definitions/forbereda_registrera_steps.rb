### Hämta icke dos-patient ###

När /^jag klickar på länken registrera samtycke$/ do
  find('span.internal', text: 'Klicka här för att registrera samtycke').click()
end

Givet /^(?:att )?jag söker namn på leveransadress "([^"]*)"$/ do |term|
  find(:ref, 'searchDoseReceiverField').set(term)
end

Givet /^(?:att )?jag söker ort på leveransadress "([^"]*)"$/ do |term|
  find('input#ReceiverCity').set(term)
end

Och /^jag väljer dosaktör "([^"]*)" "([^"]*)" från lista$/ do |levAdress, dosAkt|
  re =  Regexp.new(Regexp.escape(levAdress) + '.+' + Regexp.escape(dosAkt))
  find('table#DoseReceiverSearchResultForm tr', text: re).find('td', text: levAdress).click()
end

Och /^jag ange roll för kontaktinformation "([^"]*)"$/ do |roll|
  fill_in('ResponsibleContactFullName', :with => roll)
end

Och /^jag fyller i för kontaktinformation "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/ do |roll, adress, tel, postnr, ort|
  fill_in('ResponsibleContactFullName', :with => roll)
  fill_in('ResponsibleContactAddress', :with => adress)
  fill_in('ResponsibleContactPhoneNumber', :with => tel)
  fill_in('ResponsibleContactZipCode', :with => postnr)
  fill_in('ResponsibleContactCity', :with => ort)
end

Och /^jag ange samtycke/ do
  find("label.spaceUnder-x2.bold.au-target").click()
end

När /^jag klickar för kreditansökan$/ do
  within('div.paymentInformation.au-target') do
   find('a.external').click()
  end
  page.driver.browser.switch_to.window(page.driver.browser.window_handles.first)
end

Så /^jag markerar patienten bor hemma/ do
  find("label.spaceUnder-x2").click()
end

Och /^jag ange telefonnummer till patienten "([^"]*)"$/ do |tel|
  fill_in('PatientPhoneNumber', :with => tel)
end

Så /^meddelande om begränsad förskrivningsrtätt visas$/ do
  expect(page).to have_selector('notification-block', text: 'begränsad förskrivningsrätt')
end

Och /^jag väljer expediering "([^"]*)"$/ do |expediering|
  within('select.form-control.au-target') do
    select(expediering)
  end
end

Och /^jag anger orsak "([^"]*)"$/ do |orsak|
  within('select#RestingReason.full.au-target') do
    select(orsak)
  end
end

Och /^jag hanterar dialogen Informera patienten$/ do
  if page.has_selector?('h1', text: 'Informera patienten!', wait: 1)
    click_on('Stäng')
  end
end
