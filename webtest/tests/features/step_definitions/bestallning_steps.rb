Så /^meddelande om slutexpediering visas$/ do
  expect(page).to have_selector('notification-block', text: 'Efter leverans av beställningen kommer receptet vara slutexpedierat')
end

Så /^meddelande om ej förmån visas$/ do
  expect(page).to have_selector('notification-block', text: 'Ej förmån, patienten står för hela kostnaden')
end

Så /^jag väljer att beställa "([^"]*)" förpackningar av "([^"]*)"$/  do |fp, artikel|
  within('div.pl-modal div.actionItem', text: artikel) do
    select(fp)
  end
end

Så /^visas inte meddelande om slutexpediering$/ do
  expect(page).to have_no_selector('notification-block', text: 'Efter leverans av beställningen kommer receptet vara slutexpedierat')
end

Och /^jag noterar antalet beställningar$/ do
  tabtext = find('tab', text: 'Beställningar').text
  @bestallningar = tabtext.scan(/\d+/).first
  puts @bestallningar
end

Så /^har antalet beställningar ökat med (\d+)$/ do |antal|
  tab = find('tab', text: 'Beställningar')
  expected = @bestallningar.to_i + antal.to_i
  expect(tab).to have_content("(#{expected})")
end

Så /^finns en beställning med innehåll:$/ do |table|
  data = table.raw[0].map { |cell| Regexp.escape(cell) }
  re =  Regexp.new(data.join('.+'))
  @bestallning = find('#Orders tbody.section-body tr', text: re)
end

Så /^beställningen är markerad som akut$/ do
  expect(@bestallning).to have_selector('div.emergencySymbol')
end

Så /^jag tar bort "([^"]*)" från beställningsdialogen$/ do |artikel|
  within('div.pl-modal div.actionItem', text: artikel) do
    find('button').click()
  end
end

Så /^jag anger kommentar till dosproducent "([^"]*)"$/ do |kommentar|
  fill_in('Eventuellt meddelande till dosproducent', :with => kommentar)
end
