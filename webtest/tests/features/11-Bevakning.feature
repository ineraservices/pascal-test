#language: sv

Egenskap: 11-Bevakning
  Bevakningsfunktionen finns i det två flikar som heter Bevakade recept
  och Bevakade händelser I dessa listor visas bevakade patienter som har
  recept som håller på att gå ut eller precis har gått ut och de patienter
  som har fått ändringar i sin läkemedelslista

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    När Pascal föreslår förpackning och antalet uttag för helförpackningsrecept är vald
    Och jag rensar bevakningslistan

  Scenario: 11-1 Bevakade recept och händelser
    Givet jag ändrar intställningar för bevakning med "3" veckor
    Och jag väljer att visa bevakade händelser "Inga bevakningar / Inga markeringar"
    Och jag väljer hantering av patienter "Fråga för alla patienter jag öppnar"
    Och jag klickar "Spara & stäng"
    Och bevakade recept är tom
    Och bevakade händelser är tom
    Och icke-dos-patient är hämtad
    Och Bevakningsfunktionen är inaktiv
    Och jag stänger patientsidan
    Och dos-patient är hämtad
    Och Bevakningsfunktionen är aktiv
    Givet jag ändrar intställningar för bevakning med "3" veckor
    Och jag väljer att visa bevakade händelser "1 vecka tillbaka"
    Och jag väljer hantering av patienter "Fråga, men endast när jag gjort en förskrivningsändring"
    Och jag klickar "Spara & stäng"
    Och Bevakningsfunktionen är inaktiv
    Och jag klickar "Ny förskrivning"
    Givet jag väljer helförpackning
    När jag söker efter läkemedel "Oxynorm"
    Och jag markerar "Får ej bytas"
    Och jag väljer "OxyNorm" "Kapsel, hård" "5 mg" från lista
    Och jag klickar "Välj läkemedel"
    Och jag anger ändamål "Scenariotestfall_11-1"
    Och jag anger kortnotation "1x2"
    Och jag anger receptetsgiltighetstid 20 dagar
    Och jag klickar "Skapa & stäng"
    Och jag klickar "Bevaka patient"
    Och Bevakningsfunktionen är inaktiv
    Och jag stänger patientsidan
    Så information om utgående och utgångna recept visas med 21 dagar
    Och dospatienten visas under utgående recept
    Och dospatienten visas under bevakade händelser
    Givet jag ändrar intställningar för bevakning med "4" veckor
    Och jag väljer hantering av patienter "Lägg till patient när jag gjort en förskrivningsändring"
    Och jag klickar "Spara & stäng"
    Så information om utgående och utgångna recept visas med 28 dagar
    Och dospatienten rensas från bevakning
    Och bevakade recept är tom
    Och bevakade händelser är tom
    Och dos-patient är hämtad
    Och Bevakningsfunktionen är inaktiv
    Så finns ett aktuellt recept med innehåll:
      | OxyNorm | Scenariotestfall_11-1 |
    När jag väljer "Sätt ut" i menyn för receptet
    Och jag anger utsättningsorsak "Annan orsak"
    Och jag anger utsättningsorsak i fritext "Scenariotestfall_11-1"
    Och jag klickar "Sätt ut"
    Och jag väntar på att sidan laddas upp
    Och Bevakningsfunktionen är inaktiv
    Och jag stänger patientsidan
    Och dospatienten visas under bevakade händelser
    Och dospatienten rensas från bevakning
    Och jag loggar ut
