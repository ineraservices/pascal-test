#language: sv

Egenskap: 9-Sätt in igen
  Sätt in igen recept

  Bakgrund:
    Givet att jag är inloggad i Pascal som Läkare
    Och dos-patient är hämtad

  Scenario: 9-1 Sätt in igen ett dispenserad recept
    Givet att det finns minst ett historiskt recept med innehåll:
      | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Utsatt: Överkänslighet |
    När jag väljer "Sätt in igen" i menyn för receptet
    Så visas dialogen "Sätt in receptet igen"
      # Hantering om patienten är hemmaboende eller inte
    Och jag väljer akut produktion
    Och jag klickar "Sätt in igen"
    Så finns ett aktuellt recept med innehåll:
      | Brufen | 200 mg | Filmdragerad tablett | 08 1 + 20 1 | Scenariotestfall_3-3 |
    Och receptet är markerad som ny
    Och jag loggar ut

  Scenario: 9-2 Sätt in igen en HF till dispenserad recept
    Givet att det finns minst ett historiskt recept med innehåll:
      | Detrusitol | 2 mg | Filmdragerad tablett | 20 1 | Utsatt: Förlängning |
    När jag väljer "Sätt in igen som dospåse" i menyn för receptet
    Och visas dublettvarning
    Och dialogtexten innehåller "Läkemedlet är inte dispenserbart och har bytts mot ett annat likvärdigt dispenserbart läkemedel. Bekräfta det föreslagna läkemedlet i listan, eller välj det ursprungliga läkemedlet som helförpackning."
    Och jag markerar "Får ej bytas"
    Och jag väljer "Tolterodine Sandoz" "Depotkapsel, hård" "4 mg" från lista
    Och jag klickar "Välj läkemedel"
    Så visas dialogen "Läkemedelsspecifikation ändrad"
    Och jag klickar "Töm dosering"
    Och jag anger kortnotation "1x2 i 6 m"
    Och jag klickar "Sätt in igen"
    Så visas dialogen "Bekräfta dubbelförskrivning"
    Och jag klickar "Bekräfta"
    Så finns ett framtidrecept med innehåll:
      | Tolterodine Sandoz | 4 mg | Depotkapsel, hård | 08 1 + 20 1 | Scenariotestfall_3-10 |
    Och jag loggar ut
