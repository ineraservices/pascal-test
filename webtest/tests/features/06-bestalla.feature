#language: sv

Egenskap: 6-Beställa
  Beställningar

  Bakgrund:
    Givet att jag är inloggad i Pascal som Sjuksköterska utan förskrivningsrätt
    Och dos-patient är hämtad
    Och jag noterar antalet beställningar

  Scenario: 6-1 Beställ ett HF recept, akut
    Givet att det finns ett recept med innehåll:
      | Apocanda-Lösung | Scenariotestfall_3-9 |
    När jag väljer "Beställ" i menyn för receptet
    Så visas dialogen "Beställ"
    Och meddelande om slutexpediering visas
    Och jag väljer att beställa "1" förpackningar av "Apocanda-Lösung"
    Och jag markerar "Jag vill beställa Apocanda-Lösung akut."
    Så visas inte meddelande om slutexpediering
    Och jag klickar "Beställ"
    Så har antalet beställningar ökat med 1
    Och jag går till fliken "Beställningar"
    Så finns en beställning med innehåll:
      | Apocanda-Lösung | Kutan lösning | 1 st | Licensförpackning | Registrerad |
    Och beställningen är markerad som akut
    Och jag loggar ut

  Scenario: 6-2 Beställ ett HF recept
    Givet att det finns ett recept med innehåll:
      | Waran | Scenariotestfall_3-6 |
    När jag väljer "Beställ" i menyn för receptet
    Så visas dialogen "Beställ"
    Och jag klickar "Beställ"
    Så har antalet beställningar ökat med 1
    Och jag går till fliken "Beställningar"
    Så finns en beställning med innehåll:
      | Waran | Tablett | 2 st | Burk, 100 tabletter | Registrerad |
    Och jag loggar ut

  Scenario: 6-3 Beställ ett HF recept, akut
    Givet att det finns ett recept med innehåll:
      | Detrusitol | Scenariotestfall_3-10 |
    När jag väljer "Beställ" i menyn för receptet
    Så visas dialogen "Beställ"
    Och jag väljer att beställa "1" förpackningar av "Detrusitol"
    Och jag markerar "Jag vill beställa Detrusitol akut."
    Och jag klickar "Beställ"
    Så har antalet beställningar ökat med 1
    Och jag går till fliken "Beställningar"
    Så finns en beställning med innehåll:
      | Detrusitol | 2 mg, Filmdragerad tablett | 1 st | Burk, 60 tabletter | Registrerad |
    Och beställningen är markerad som akut
    Och jag loggar ut

  Scenario: 6-4 Multi-beställ HV recept
    Givet Handelsvaror fliken är vald
    När jag markerar receptet med innehåll:
      | Teknisk Sprit | Scenariotestfall_3-11 |
    Och jag markerar receptet med innehåll:
      | Cutisoft Wipes | Scenariotestfall_3-12 |
    Och jag markerar receptet med innehåll:
      | Natriumklorid | Scenariotestfall_3-13 |
    Och jag klickar "Beställ"
    Så visas dialogen "Beställ"
    Och meddelande om slutexpediering visas
    Och meddelande om ej förmån visas
    Och jag markerar "Gemensam apotekskommentar och akutbeställning"
    Och jag tar bort "Cutisoft Wipes" från beställningsdialogen
    Och jag väljer att beställa "1" förpackningar av "Teknisk Sprit"
    Så visas inte meddelande om slutexpediering
    Och jag markerar "Jag vill beställa akut."
    Och jag anger kommentar till dosproducent "Scenariotestfall_6-3"
    Och jag klickar "Beställ" i dialogen
    Så har antalet beställningar ökat med 2
    Och jag går till fliken "Beställningar"
    Så finns en beställning med innehåll:
      | Teknisk Sprit | 1 st | Registrerad |
    Så finns en beställning med innehåll:
      | Natriumklorid | 2 st | Registrerad |
    Och jag loggar ut
