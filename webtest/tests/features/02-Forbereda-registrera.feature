#language: sv

Egenskap: 2-Förbereda och registrera
  Förbereda och registrera en patient med olika roller och behörigheter

  Scenario: 2-1 Förbered en patient som sjuksköterska med förskrivningsrätt
    Givet att jag är inloggad i Pascal som Sjuksköterska
    Och icke-dos-patient är hämtad
    Och jag klickar "Förbered som dospatient"
    Så visas dialogen "Förbered patient"
    Och jag klickar "Förbered"
    Så visas dialogen "Förbered patient"
    Givet att jag söker namn på leveransadress "Ineras ORDBO"
    Och jag klickar "Sök"
    Och jag väljer dosaktör "Ineras ORDBO Testvägen 2 11112 Teststaden" "Svensk Dos AB" från lista
    Och jag ange roll för kontaktinformation "SSK"
    Och jag klickar "Förbered"
    Så visas dialogen "Patienten är förberedd"
    Och jag klickar "Stäng"
    Och jag klickar "Förbered som dospatient"
    Så visas dialogen "Förbered patient"
    Och jag klickar "Ta bort förberedd data"
    Så visas dialogen "Ta bort förberedd data"
    Och jag klickar "Ta bort"
    Så visas dialogen "Ta bort förberedd data"
    Och jag klickar "Stäng"
    Och jag loggar ut

  Scenario: 2-2 Förbered en patient som sjuksköterska utan förskrivningsrätt
    Givet att jag är inloggad i Pascal som Sjuksköterska utan förskrivningsrätt
    Och icke-dos-patient är hämtad
    Och jag klickar "Förbered som dospatient"
    Så visas dialogen "Förbered patient"
    Och jag klickar "Förbered"
    Så visas dialogen "Förbered patient"
    Givet att jag söker ort på leveransadress "Teststaden"
    Och jag klickar "Sök"
    Och jag väljer dosaktör "Ineras ORDBO Testvägen 2 11112 Teststaden" "Svensk Dos AB" från lista
    Och jag klickar "Förbered"
    Så visas dialogen "Patienten är förberedd"
    Och jag klickar "Stäng"
    Och jag loggar ut

  Scenario: 2-3 Ändra och förbered en patient som administratör
    Givet att jag är inloggad i Pascal som Administratör
    Och icke-dos-patient är hämtad
    Och jag klickar "Förbered som dospatient"
    Så visas dialogen "Förbered patient"
    Och jag klickar "Ändra & komplettera"
    Så visas dialogen "Förbered patient"
    Och jag fyller i för kontaktinformation "SSK Testare" "Testgatan 3" "0811223344" "12345" "Stockholm"
    När jag klickar för kreditansökan
    Och jag klickar "Förbered"
    Så visas dialogen "Patienten är förberedd"
    Och jag klickar "Stäng"
    Och jag loggar ut

      #  Detta testfall körs inte pga det registrerar en ny testpatient
  @wip
  Scenario: 2-4 Ändra, registrera en patient och förskriva recept som läkare
    Givet att jag är inloggad i Pascal som Läkare
    Och icke-dos-patient är hämtad
    Och jag klickar "Registrera som dospatient"
    Och jag ange samtycke
    Och jag klickar "Registrera"
    Så visas dialogen "Misslyckades att registrera patient"
    Och jag klickar "Stäng" i dialogen
    Och jag klickar "Ändra & komplettera"
    Så jag markerar patienten bor hemma
    # Patientens uppgifter  är borta i 2.3 då det inte längre är något som man kan fylla i
    #som användare i SOL 17.1. eHM kommer istället själva fylla i den infon från FOLK
    #    Och jag ange telefonnummer till patienten "07045362819"
    Och jag klickar "Registrera"
    Och jag klickar "Ny förskrivning"
    Givet att jag söker efter läkemedel "Nitrofurantoin"
    Så visas äldrevarning
    Och jag klickar "Välj läkemedel"
    Och jag anger ändamål "Scenariotestfall_3-2"
    Och jag väljer oregelbunden dosering med schema "Annan oregelbunden dosering"
    Och jag väljer när schemat nått sista dagen, ska schemat "inte upprepas"
    Och jag klickar "Välj oregelbunden dosering"
    Och jag väljer antal dagar i schema "7 dagar (veckoschema)"
    Och jag fyller i för alla dagar i schemat för annan orgelbunden dosering:
      | 08  | 12 | 16  | 20 |
      | 0,5 |    | 0,5 |    |
    Och jag anger förmån "Ja"
    Och jag klickar "Skapa & stäng"
    Så finns ett framtidrecept med innehåll:
      | Nitrofurantoin Alternova | 50 mg | Tablett | 08 D | 16 D | oregelbunden | Scenariotestfall_3-2 |
    Och receptet är markerad som ny
    Och jag loggar ut
