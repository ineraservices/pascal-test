pipeline {
    agent {
        label 'compose-slave'
    }

    stages {
        stage('Web GUI testing') {
            steps {
                // Re-build if the environment has been updated (tests and output excluded by .dockerignore)
                sh 'cd webtest && docker-compose build'
                // Run the tests and output two reports, one in junit-format and one in json (for cucumber-reports-plugin).
                // Empty DEBUG-variable makes the standalone-browser not run with a VNC-server.
                sh 'cd webtest && DEBUG="" docker-compose run --rm testsuite --tags "not @wip" -f pretty -f junit -o junit-reports -f json -o report.json'
            }
        }

    }

    post {
        always {

            // Change ownership of output files to Jenkins user
            sh 'cd webtest && docker-compose run --rm --entrypoint ./chown-files testsuite'
            // Clean up webtests
            sh 'cd webtest && docker-compose stop && docker-compose rm -f'
            // Archive webtest results
            junit healthScaleFactor: 100.0, testResults: 'webtest/tests/junit-reports/*.xml'
            cucumber 'webtest/tests/report.json'

        }
    }
}
